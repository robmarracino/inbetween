package sample;

/**
 * /**
 *  * Game Driver Class
 *  * @author Roberto, Weston, Shadi
 *  */


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.Window;
import sample.Card;
import sample.House;
import sample.Person;

public class Main extends Application {

    Stage Window;
    Scene firstWindow;
    Scene secondWindow;

    Label headerLabel;
    TextField playerOneField;
    Label playerOne;
    TextField playerTwoField;
    Label playerTwo;
    TextField playerThreeField;
    Label playerThree;
    TextField playerFourField;
    Label playerFour;
    static public String first;
    static public String second;
    static public String third;
    static public String fourth;
    public double startingBankroll = 12.50;
    public double p1BankRoll;
    public double p2BankRoll;
    public double p3BankRoll;
    public double p4BankRoll;

    Button submit;
    Label welcome;
    Label player1;
    Label player2;
    Label player3;
    Label player4;
    Label labelForCurrentPlayer;
    Label currentPot;
    Label player1BankRoll;
    Label player1BankRollAmount;
    Label player2BankRoll;
    Label player2BankRollAmount;
    Label player3BankRoll;
    Label player3BankRollAmount;
    Label player4BankRoll;
    Label player4BankRollAmount;
    Label placeBet;
    public TextField amountToBet;
    Button submitBet;


    static Person person1 = new Person(first);
    static Person person2 = new Person(second);
    static Person person3 = new Person(third);
    static Person person4 = new Person(fourth);
    static Person currentPlayer;
    static House house = new House(1, 4);


    @Override
    public void start(Stage primaryStage) throws Exception {

        /**
         *  THIS IS THE START OF THE FIRST WINDOW THAT POPS UP, I AM IN THE PROCESS OF COMBINGING THE TWO. LOOK
         *  HERE FOR ANY ERRORS IN THE FIRST WINDOW
         */

        Window = primaryStage;

        /**
         *  Instantiate a new Grid Pane for the first window that appears
         */
        GridPane gridPane = new GridPane();

        /**
         * Position the pane at the center of the screen, both vertically and horizontally
         */
        gridPane.setAlignment(Pos.CENTER);

        /**
         * Set a padding of 20px on each side
         */
        gridPane.setPadding(new Insets(40, 40, 40, 40));

        /**
         *  Set the horizontal gap between columns
         */
        gridPane.setHgap(10);

        /**
         * Set the vertical gap between rows
         */
        gridPane.setVgap(10);

        // Add Column Constraints

        /**
         * columnOneConstraints will be applied to all the nodes placed in column one.
         */
        ColumnConstraints columnOneConstraints = new ColumnConstraints(100, 100, Double.MAX_VALUE);
        columnOneConstraints.setHalignment(HPos.RIGHT);

        /**
         * columnTwoConstraints will be applied to all the nodes placed in column two.
         */
        ColumnConstraints columnTwoConstrains = new ColumnConstraints(200, 200, Double.MAX_VALUE);
        columnTwoConstrains.setHgrow(Priority.ALWAYS);

        /**
         * Adding the constraints to the Grid Pane that we created for the first window
         */
        gridPane.getColumnConstraints().addAll(columnOneConstraints, columnTwoConstrains);


        /**
         * This is creating the Header Label for the first Window and setting the font, and font size
         * as well as the alignment settings
         */
        headerLabel = new Label("In Between Player Names");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        gridPane.add(headerLabel, 0, 0, 2, 1);
        GridPane.setHalignment(headerLabel, HPos.CENTER);
        GridPane.setMargin(headerLabel, new Insets(20, 0, 20, 0));

        /**
         * here we are creating that informs the user to enter the first players name
         */
        playerOne = new Label("Player 1 Name : ");
        gridPane.add(playerOne, 0, 1);

        /**
         * Here we are creating a text field that will go next to the "player 1 name label"
         * The user will enter his or hers name here
         */
        playerOneField = new TextField();
        playerOneField.setPrefHeight(40);
        playerOneField.setPrefWidth(300);
        playerOneField.setMaxWidth(300);
        playerOneField.setTranslateX(100);
        gridPane.add(playerOneField, 1, 1);

        /**
         * here we are creating that informs the user to enter the Second players name
         */
        playerTwo = new Label("Player 2 Name : ");
        gridPane.add(playerTwo, 0, 2);

        /**
         * Here we are creating a text field that will go next to the "player 2 name label"
         * The user will enter his or hers name here
         */
        playerTwoField = new TextField();
        playerTwoField.setPrefHeight(40);
        playerTwoField.setPrefWidth(300);
        playerTwoField.setMaxWidth(300);
        playerTwoField.setTranslateX(100);
        gridPane.add(playerTwoField, 1, 2);

        /**
         * here we are creating that informs the user to enter the Third players name
         */
        playerThree = new Label("Player 3 Name : ");
        gridPane.add(playerThree, 0, 3);

        /**
         * Here we are creating a text field that will go next to the "player 3 name label"
         * The user will enter his or hers name here
         */
        playerThreeField = new TextField();
        playerThreeField.setPrefHeight(40);
        playerThreeField.setPrefWidth(300);
        playerThreeField.setMaxWidth(300);
        playerThreeField.setTranslateX(100);
        gridPane.add(playerThreeField, 1, 3);

        /**
         * here we are creating that informs the user to enter the Fourth players name
         */
        playerFour = new Label("Player 4 Name : ");
        gridPane.add(playerFour, 0, 4);

        /**
         * Here we are creating a text field that will go next to the "player 4 name label"
         * The user will enter his or hers name here
         */
        playerFourField = new TextField();
        playerFourField.setPrefHeight(40);
        playerFourField.setPrefWidth(300);
        playerFourField.setMaxWidth(300);
        playerFourField.setTranslateX(100);
        gridPane.add(playerFourField, 1, 4);


        /**
         * creating a new scene for the first window and placing the gridpane inside that scene
         * we are also setting first scene of the PrimaryStage(window) to our current scene then showing
         * the primary stage
         */
        firstWindow = new Scene(gridPane, 800, 500);
        Window.setTitle("In Between");
        Window.setScene(firstWindow);
        Window.show();


        /**
         * Here we created a button that does two things. First upon pressing it closes the current window and
         * opens a new one that will be the next scene (this will be the main game GUI)
         * Second it will take the user input player names and tranfer them to labels that are created in the next scene
         * these labels will display whatever the user inputed into the text field.
         */
        Button submitButton = new Button("Submit");
        submitButton.setPrefHeight(40);
        submitButton.setDefaultButton(true);
        submitButton.setPrefWidth(100);
        gridPane.add(submitButton, 0, 5, 2, 1);
        GridPane.setHalignment(submitButton, HPos.CENTER);
        GridPane.setMargin(submitButton, new Insets(20, 0, 20, 0));
        submitButton.setOnAction(e -> {
            first = playerOneField.getText();
            player1.setText(first);
            second = playerTwoField.getText();
            player2.setText(second);
            third = playerThreeField.getText();
            player3.setText(third);
            fourth = playerFourField.getText();
            player4.setText(fourth);
            Window.setScene(secondWindow);
        });


        /**
         * THIS IS THE START OF THE MAIN GRID PAN WINDOW. lOOK HERE FOR ANY ERRORS IN THE ACTUAL PLAYING TABLE
         */

        /**
         * This is a new Gridpane created for the second window (the actual game window)
         */
        GridPane root = new GridPane();
        root.setAlignment(Pos.CENTER);
        root.setVgap(10);
        root.setHgap(10);

        /**
         * Here we are creating the new scene that will be the maine game window
         */

        secondWindow = new Scene(root, 1800, 1200);

        /**
         * This is the First Card that will be drawn from the deck with an image to represent it
         * For all three of these, they are currently static images. While this is not the intent we are not able to get
         * the gui working wit the different card images correctly. I mainly put these here for you to see that we did
         *  finish the GUI with the images in place.
         */
        Image pic1 = new Image("cards_AceDiamonds.png");
        ImageView imageview = new ImageView();
        imageview.setImage(pic1);
        imageview.setFitWidth(115);
        imageview.setFitHeight(115);
        imageview.setTranslateX(-60);
        root.add(imageview, 0, 4);

        /**
         * This is the Second Card that will be drawn from the deck with an image to represent it
         *
         * For all three of these, they are currently static images. While this is not the intent we are not able to get
         * the gui working wit the different card images correctly. I mainly put these here for you to see that we did
         * finish the GUI with the images in place.
         */
        Image pic2 = new Image("cards_AceClubs.png");
        ImageView imageview1 = new ImageView();
        imageview1.setImage(pic2);
        imageview1.setFitWidth(115);
        imageview1.setFitHeight(115);
        imageview1.setTranslateX(100);
        root.add(imageview1, 0, 4);

        /**
         * This is the Third Card that will be drawn from the deck with an image to represent it
         *
         * For all three of these, they are currently static images. While this is not the intent we are not able to get
         * the gui working wit the different card images correctly. I mainly put these here for you to see that we did
         * finish the GUI with the images in place.
         */
        Image pic3 = new Image("cards_AceHearts.png");
        ImageView imageview2 = new ImageView();
        imageview2.setImage(pic3);
        imageview2.setFitWidth(115);
        imageview2.setFitHeight(115);
        imageview2.setTranslateX(-150);
        root.add(imageview2, 0, 4);


        /**
         * This is the HBOX that holds all the card images and it is placed in the middle
         */
        HBox cards = new HBox();
        cards.setAlignment(Pos.CENTER);
        cards.getChildren().add(imageview);
        cards.getChildren().add(imageview1);
        cards.getChildren().add(imageview2);
        cards.setTranslateY(-200);
        cards.setTranslateX(50);
        root.add(cards, 0, 5);

        /**
         * this is a Label for Player1 Name
         */
        player1 = new Label();
        player1.setTranslateY(-400);
        player1.setAlignment(Pos.CENTER);
        player1.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(player1, 0, 1);

        /**
         * this is a created for Player1 Bankroll total text string
         */
        player1BankRoll = new Label("BankRoll:");
        player1BankRoll.setTranslateY(-385);
        player1BankRoll.setAlignment(Pos.CENTER);
        player1BankRoll.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(player1BankRoll, 0, 2);

        /**
         * this is a label created for Player1 Bankroll total numerically
         */
        player1BankRollAmount = new Label("");
        player1BankRollAmount.setTranslateY(-387);
        player1BankRollAmount.setTranslateX(-145);
        player1BankRollAmount.setAlignment(Pos.CENTER);
        player1BankRollAmount.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        player1BankRollAmount.setText(String.format("%.2f", startingBankroll));
        root.add(player1BankRollAmount, 1, 3);

        /**
         * This is a VBOX created to house the Player1 info and center it accordingly
         */

        VBox vbox1 = new VBox(5, player1, player1BankRoll);
        vbox1.setAlignment(Pos.CENTER);
        vbox1.setTranslateY(100);
        vbox1.setPadding(new Insets(10));
        root.add(vbox1, 0, 1);


        /**
         * this is a label created for Player2 Name
         */

        player2 = new Label();
        player2.setAlignment(Pos.CENTER);
        player2.setTranslateX(-350);
        player2.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(player2, 0, 2);

        /**
         * this is a label created for Player2 bankroll text String
         */

        player2BankRoll = new Label("BankRoll:");
        player2BankRoll.setTranslateX(-350);
        player2BankRoll.setAlignment(Pos.CENTER);
        player2BankRoll.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(player2BankRoll, 0, 3);

        /**
         * this is a created for the Label for Player2 bankroll total numerically
         */

        player2BankRollAmount = new Label("");
        player2BankRollAmount.setTranslateY(-135);
        player2BankRollAmount.setTranslateX(825);
        player2BankRollAmount.setAlignment(Pos.CENTER);
        player2BankRollAmount.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        player2BankRollAmount.setText(String.format("%.2f", startingBankroll));
        root.add(player2BankRollAmount, 0, 3);

        /**
         * This is a VBOX created to house the Player2 info and center it accordingly
         */

        VBox vbox3 = new VBox(10, player2, player2BankRoll);
        vbox3.setAlignment(Pos.CENTER);
        vbox3.setTranslateY(-150);
        vbox3.setTranslateX(-250);
        vbox3.setPadding(new Insets(10));
        root.add(vbox3, 0, 3);

        /**
         * this is a label created for Player3 Name
         */

        player3 = new Label(null);
        player3.setAlignment(Pos.CENTER);
        player3.setTranslateX(350);
        player3.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(player3, 0, 2);

        /**
         * this is a label created for Player3 bankroll text String
         */


        player3BankRoll = new Label("BankRoll:");
        player3BankRoll.setTranslateX(350);
        player3BankRoll.setAlignment(Pos.CENTER);
        player3BankRoll.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(player3BankRoll, 0, 3);

        /**
         * this is a created for the Label for Player3 bankroll total numerically
         */

        player3BankRollAmount = new Label("");
        player3BankRollAmount.setTranslateY(-135);
        player3BankRollAmount.setTranslateX(-380);
        player3BankRollAmount.setAlignment(Pos.CENTER);
        player3BankRollAmount.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        player3BankRollAmount.setText(String.format("%.2f", startingBankroll));
        root.add(player3BankRollAmount, 0, 3);

        /**
         * This is a VBOX created to house the Player3 info and center it accordingly
         */

        VBox vbox4 = new VBox(10, player3, player3BankRoll);
        vbox4.setAlignment(Pos.CENTER);
        vbox4.setTranslateY(-150);
        vbox4.setTranslateX(250);
        vbox4.setPadding(new Insets(10));
        root.add(vbox4, 0, 3);

        /**
         * this is a label created for Player4 Name
         */

        player4 = new Label(null);
        player4.setAlignment(Pos.CENTER);
        player4.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(player4, 0, 4);

        /**
         * this is a label created for Player4 bankroll text String
         */

        player4BankRoll = new Label("BankRoll:");
        player4BankRoll.setAlignment(Pos.CENTER);
        player4BankRoll.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(player4BankRoll, 0, 4);

        /**
         * this is a created for the Label for Player4 bankroll total numerically
         */

        player4BankRollAmount = new Label("");
        player4BankRollAmount.setTranslateY(215);
        player4BankRollAmount.setTranslateX(225);
        player4BankRollAmount.setAlignment(Pos.CENTER);
        player4BankRollAmount.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        player4BankRollAmount.setText(String.format("%.2f", startingBankroll));
        root.add(player4BankRollAmount, 0, 3);

        /**
         * This is a VBOX created to house the Player4 info and center it accordingly
         */

        VBox vbox6 = new VBox(10, player4, player4BankRoll);
        vbox6.setAlignment(Pos.BOTTOM_CENTER);
        vbox6.setTranslateY(200);
        vbox6.setPadding(new Insets(10));
        root.add(vbox6, 0, 3);


        /**
         * created Label that Says "Welcome to In Between" and placed in the top left corner
         */

        welcome = new Label("Welcome to In-Between");
        welcome.setAlignment(Pos.CENTER);
        welcome.setTranslateY(-300);
        welcome.setFont(Font.font("Time New Roman", FontWeight.BOLD, 25));
        root.add(welcome, 0, 3);

        /**
         * Label that notes whos turn it is (We were not able to get this working but I wanted you to know that
         * the label was made and put there)
         */
        labelForCurrentPlayer = new Label("Current Player:" + currentPlayer);
        labelForCurrentPlayer.setAlignment(Pos.CENTER);
        labelForCurrentPlayer.setTranslateY(-300);
        labelForCurrentPlayer.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(labelForCurrentPlayer, 0, 4);

        /**
         * This label notes the total of the current POT and is housed the top left the screen
         */
        currentPot = new Label("Current Pot:" + house.getPot());
        currentPot.setAlignment(Pos.CENTER);
        currentPot.setTranslateY(-300);
        currentPot.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(currentPot, 0, 5);

        /**
         * This was a VBox created to house the Welcome, currentPot, and Current player label
         * They were centered in the VBOX and all placed in the top left corner of the window
         */
        VBox vbox2 = new VBox(10, welcome, currentPot, labelForCurrentPlayer);
        vbox2.setAlignment(Pos.CENTER);
        vbox2.setTranslateY(-50);
        vbox2.setTranslateX(-550);
        vbox2.setPadding(new Insets(10));
        root.add(vbox2, 0, 1);


        /**
         * This label is just a text string that tells the user to place there bet in the bottom left corner of the screen.
         */
        placeBet = new Label("Place your bet and click submit");
        placeBet.setAlignment(Pos.CENTER);
        placeBet.setTranslateY(325);
        placeBet.setTranslateX(-550);
        placeBet.setFont(Font.font("Time New Roman", FontWeight.BOLD, 15));
        root.add(placeBet, 0, 6);


        /**
         * This was an HBOX created to house the Text Field that is for the user to enter the amount of money the would like to bet
         */
        HBox hbForTextField = new HBox(10);
        amountToBet = new TextField();
        hbForTextField.setAlignment(Pos.CENTER);
        hbForTextField.setTranslateY(100);
        hbForTextField.setTranslateX(-550);
        hbForTextField.getChildren().add(amountToBet);
        root.add(hbForTextField, 0, 6);

        /**
         * This is the submit bet button, this button is supposed to interact with the PlayCard method and but we could not
         * get the action event to work correctly. I placed it there to let you know it was done and put in place in the GUI
         */
        submitBet = new Button("Submit");
        HBox button = new HBox(10);
        button.setAlignment(Pos.CENTER);
        button.setTranslateY(100);
        button.setTranslateX(-550);
        button.getChildren().add(submitBet);
        root.add(button, 0, 7);

        /**
         * THis was a VBOX created to house the PLACE BET label, this made it easier to move the object around the Grid.
         */
        VBox vbox5 = new VBox(15, placeBet);
        vbox5.setAlignment(Pos.BOTTOM_CENTER);
        vbox5.setPadding(new Insets(10));
        root.add(vbox5, 0, 1);

    }

    /**
     *
     * Action event created to interact with the submit bet button.
     * @param submitBet
     */
    public void handle(ActionEvent submitBet) {
        boolean done = false;
        do {
            try {
                double betAmountEntered = Double.parseDouble(amountToBet.getText());
                isValid(betAmountEntered);
                done = true;
                currentPlayer.BetAmount(betAmountEntered);
            } catch (IllegalBetException e) {
            }
        } while (!done);
    }

    /**
     * Illegal bet exception created to let the user know his or hers best must be 0 or above
     * @param bet
     * @return returns true if the bet is above zero, else throws
     * @throws IllegalBetException
     */
    public boolean isValid(double bet) throws IllegalBetException {
        if (bet > currentPlayer.bankroll) {
            IllegalBetException e = new IllegalBetException("Can't bet more than you have");
            throw e;
        } else if (bet <= 0) {
            IllegalBetException e = new IllegalBetException("Can't bet 0 or below");
        }
        return true;
    }

    /**
     * This is  Display Card method that we attempted to use but could not get working properly, while it does not do anything of use as it is not called,
     * I wanted to see the direction we were heading.
     * @param card
     */
    public static void displayCard(Card card) {
        int cardPosition = 0;
        Image cardPic = new Image("cards/cards" + card.fileName);
        ImageView imageview = new ImageView();
        imageview.setImage(cardPic);
        imageview.setFitWidth(170);
        imageview.setFitHeight(220);
        if (cardPosition == 0) {
            imageview.setTranslateX(-400);
            cardPosition++;
        } else if (cardPosition == 1) {
            imageview.setTranslateX(400);
            cardPosition++;
        } else {
            imageview.setTranslateX(100);
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
        person1.BuyIn();
        person2.BuyIn();
        person3.BuyIn();
        person4.BuyIn();
        boolean running = true;
        while (running) {
            if (currentPlayer.activePlayer == true) {
                house.PlayHand(currentPlayer);
            }
            if (currentPlayer == person1) {
                currentPlayer = person2;
            } else if (currentPlayer == person2) {
                currentPlayer = person3;
            } else if (currentPlayer == person3) {
                currentPlayer = person4;
            } else {
                currentPlayer = person1;
            }
            if (house.pot <= 0) {
                running = false;
            }
        }
    }
}