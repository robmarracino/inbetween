package GameDriver;

/**
 *
 * @author robma
 */
public class IllegalBetException extends Exception {

    /**
     * Creates a new instance of <code>IllegalBetException</code> without detail
     * message.
     */
    public IllegalBetException() {
    }

    /**
     * Constructs an instance of <code>IllegalBetException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public IllegalBetException(String msg) {
        super(msg);
    }
}
