package GameDriver;

import java.util.Random;

/**
 * Deck Object collection of cards in array stack
 * @author Roberto, Weston, Shadi
 */
public class Deck {
    public Card[] cardDeck = new Card[52]; // array of 52 cards
    public int remainingCards = 52; // int value holding remaining cards
    
    /**
     * Deck constructor. 
     * Creates 52 unique cards of every rank and suit.
     */
    public Deck() {
        int i = 0; // int used to keep track of the index of a new card being added
        for (Card.Suit suit : Card.Suit.values()) { // loops through each suit value
            for (Card.Rank rank : Card.Rank.values()) { // loops through each rank value
                cardDeck[i] = new Card(rank, suit);  // creates a card of unique rank and suit and stores it into an array of cards at specified index
                i++; // increases index
            }
        }
    }
    
    /**
     * prints every card in deck collection using cards toString method
     * @return str containing every card in it's own line
     */
    public String toString() {
        String str = "";
        for (int i = 0; i < remainingCards; i++) {
            str += cardDeck[i] + "\n";
        }
        return str;
    }
    
    /**
     * Shuffle swaps a random card with an i-th card 52 times ensuring a well shuffled deck
     */
    public void shuffle() {
        Random rand = new Random(); // new random object
        
        for (int i = 0; i < remainingCards; i++) {
            int randSwapIndex = rand.nextInt(remainingCards); // int value using random object to create a random int between 0 and 52
            Card temp = cardDeck[randSwapIndex]; //swap
            cardDeck[randSwapIndex] = cardDeck[i];
            cardDeck[i] = temp;
        }
    }
    
    /**
     * getRemainingCards gets the int value of the remaining cards in the deck
     * @return remainingCards
     */
    public int getRemainingCards() {
        return remainingCards;
    }
    
    /**
     * hasCard searches the remaining cards in the deck for a specific card
     * @param search card we are searching for
     * @return boolean value true if we find the card in the deck
     */
    public boolean hasCard(Card search) {
        for (int i = 0; i < remainingCards; i++) {
            if (cardDeck[i].equals(search)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * deals the cards for the game, top being the first card drawn
     * @return the top card drawn
     */
    
    public Card deal() {
        if (remainingCards == 0) {
            remainingCards = 52;
            shuffle();
        }
        Card top = cardDeck[remainingCards-1];
        remainingCards--;
        return top;
    }
}

class Card {
    public enum Suit {
    Spades, Hearts, Diamonds, Clubs;
    }
    public enum Rank {
    Ace, two, three, four, five, six, seven, eight, nine, ten, Jack, Queen, King;
    }
    public final Suit suit;
    public final Rank rank;
    public final String fileName;
    
    /**
     * card that passes in the rank and suit of the card
     * @param rank different types of card, from Ace through King
     * @param suit which suit the card belongs to, spades, clubs, etc
     */
    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
        this.fileName = rank.name() + suit.name() + ".png";
    }

    /**
     * Card toString method
     */
     
    @Override
    public String toString() {
        return rank + " of " + suit;
    }
    
    /**
     * card equals method
     */
     
    public boolean equals(Card other) {
        return this.rank == other.rank && this.suit == other.suit;
    }
    
    /**
     * compares cards to each other
     * @param other card that the first card is being compared to
     * @return whether card matches, is less than, or greater than the other
     */
     
    public int compareTo(Card other) {
        if (this.rank.ordinal() == other.rank.ordinal()) {
            return 0;
        }
        if (this.rank.ordinal() < other.rank.ordinal()) {
            return 1;
        }
        if (this.rank.ordinal() > other.rank.ordinal()) {
            return -1;
        }
        return 5;
    }
    
    /**
     * displays an ASCII representation of the card
     * @return the ASCII art of the card
     */
     
    public String displayCard() {
        char type = ' ';
        String rankD = "";
        String display = "";
        if (rank.ordinal() == 1) {
            return "";
        }
        if (this.suit == Suit.Spades) {
            type = '♠';
        } else if (this.suit == Suit.Clubs) {
            type = '♣';
        } else if (this.suit == Suit.Hearts) {
            type = '♥';
        } else if (this.suit == Suit.Diamonds) {
            type = '♦';
        }
        if (rank.ordinal() == 0 || rank.ordinal() > 9) {
            rankD = "" + rank.name().charAt(0);
        } else {
            rankD = "" + rank.ordinal();
        }
        return "┌──────────┐\n"+
               "│ " + rankD + "              │\n"+
               "│                │\n"+
               "│                │\n"+
               "│                │\n"+
               "│       " + type + "        │\n"+
               "│                │\n"+
               "│                │\n"+
               "│                │\n"+
               "│              " + rankD + " │\n"+
               "└──────────┘\n";
    }
}