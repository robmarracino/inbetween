package gamedriver;

/**
 *
 * @author Robbie, Weston, Shadi
 */
public class Person {
    public String name; //name for the player
    public double bankroll = 12.5;  //starting bankroll
    public double bet;  //value of the player's bet
    public boolean activePlayer;    //whether player is actively playing

    /**
     * passes name to this class
     * @param name of the player
     */

    public Person(String name) {
        this.name = name;
    }
    
    /**
     * winning hand will add the winnings to the player's bankroll
     */
    
    public void WinningHand() {  // To Be Implemented
        this.bankroll += bet * 2;
    }
    
    /**
     * losing hand will subtract from the player's bankroll
     * @param multiplier if the boundary card is drawn as the 3rd card
     */
    
    public void LosingHand(int multiplier) {  // To Be Implemented
        this.bankroll -= bet * multiplier;
        if (bankroll <= 0) {
            activePlayer = false;
        }
    }
    
    /**
     * buy in for the active players
     */
    
    public void BuyIn() {   // To Be Implemented
        this.bankroll -= 1;
        activePlayer = true;
    }
    
    /**
     * takes the amount of the bet from the player
     */
    
    public void BetAmount(Double amount) {   // To Be Implemented
        // GET BET FROM GUI
        this.bet = amount;
    }

    /**
     * getter for the player name
     * @return player's name
     */

    public String getName() {
        return name;
    }

    /**
     * getter for the player's bankroll
     * @return player's bankroll
     */

    public Double getBankroll() {
        return bankroll;
    }

    /**
     * getter for the player's bet
     * @return player's bet
     */

    public Double getBet() {
        return bet;
    }

    /**
     * check if the player is still playing
     * @return if player is active
     */

    public boolean isActivePlayer() {
        return activePlayer;
    }
    
    /**
     * checks to see if multiple players have the same name
     * @param other the other player being compared to
     * @return whether it's the same or not
     */
    
    public boolean equals(Person other) {  
        if (other.name == this.name) {
            return true;
        }
        return false;
    }
    
    /**
     * compares the bankroll values of each player to each other
     * @param person1 first player
     * @param person2 second player
     * @param person3 third player
     * @param person4 fourth player
     */
     
    public void compareTo(Person person1, Person person2, Person person3, 
                          Person person4) {   // To Be Implemented
        double winning = Math.max(person1.bankroll, Math.max(person2.bankroll,
                         Math.max(person3.bankroll, person4.bankroll)));
    }
}
