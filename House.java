package gamedriver;

/**
 *
 * @author Robbie, Weston, Shadi
 */
public class House {
    public double pot; // amount of the pot in play
    public Deck mainDeck = new Deck(); // the card deck being used
    public Card card1; //first card drawn
    public Card card2; //second card drawn
    public Card card3; //third card drawn
    
    /**
     * constructor that recieves the amount of the buyin and sets the pot
     * (number of players times buyin)
     * @param buyIn buy in for the players
     * @param playerCount number of players
     */
    public House(int buyIn, int playerCount) {  
        pot = buyIn * playerCount; //sets the value of the starting pot
    }
    
    /**
     * plays the cards for the game
     * @param currentPlayer the player's turn
     */
    public void PlayHand(Person currentPlayer) { 
        Card card1; //first card drawn
        Card card2; //second card drawn
        Card card3; //third card drawn
        do {
            
            card1 = mainDeck.deal();  // DISPLAY CARD ON GUI
            setCard1(card1);
            card2 = mainDeck.deal();  // DISPLAY CARD ON GUI
            setCard2(card2);
            if (card1.compareTo(card2) == 0) {
            }
        } while (card1.compareTo(card2) != 0);
        
        // get bet
        getBet(currentPlayer);
        // deal third card
        card3 = mainDeck.deal();    // DISPLAY CARD ON GUI
        setCard3(card3);
        // adjust pot and players bankroll depending on results
        if (card3.compareTo(card1) == 0 || card3.compareTo(card2) == 0) {
            // double loss
            currentPlayer.LosingHand(2);
            this.pot -= currentPlayer.bet * 2;
        } else if ((card3.compareTo(card1) == 1 && card3.compareTo(card2) == -1) ||
                (card3.compareTo(card1) == -1 && card3.compareTo(card2) == 1)) {
            //winning hand
            currentPlayer.WinningHand();
            this.pot += currentPlayer.bet;
        } else {
            // regular losing hand
            currentPlayer.LosingHand(1);
            this.pot -= currentPlayer.bet;
        } 
        
    }
    
    /**
     * sets the card1 variable to the card drawn in PlayHand method
     * @param card1 the first card drawn
     */
    
    public void setCard1(Card card1) {
        this.Card1 = card1;
    }
    
    /**
     * sets the card2 variable to the card drawn in PlayHand method
     * @param card2 the second card drawn
     */
    
    public void setCard2(Card card2) {
        this.Card2 = card2;
    }
    
    /**
     * sets the card3 variable to the card drawn in PlayHand method
     * @param card3 the third card drawn
     */
    
    public void setCard3(Card card3) {
        this.Card3 = card3;
    }
    
    /**
     * getter that returns the first card
     * @return first card drawn
     */
    
    public Card getCard1() {
        return Card1;
    }
    
    /**
     * getter that returns the second card
     * @return second card drawn
     */
    
    public Card getCard2() {
        return Card2;
    }
    
    /**
     * getter that returns the third card
     * @return third card drawn 
     */
    
    public Card getCard3() {
        return Card3;
    }
    
    /**
     * getter that returns the value of the pot
     * @return the pot value
     */
    
    public double getPot() {
        return this.pot;
    }
    
    /**
     * asks player for a bet. it will keep asking until a valid bet is placed.
     * must be able to handle an illegalbet exception (TRY CATCH)
     * @param player
     * @return valid bet
     */
    public double getBet(Person player) { // To Be Implemented
        return player.getBet();
    }
}
